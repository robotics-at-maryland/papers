FROM debian:latest

RUN apt-get update -q && apt-get upgrade -qy

RUN apt-get install -y texlive-full

ENV HOME /data
WORKDIR /data
VOLUME ["/data"]
