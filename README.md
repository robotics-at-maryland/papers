# papers

## Building

```
pdflatex main.tex
```

Outputs to `main.pdf`.

### Building with Docker

If you have Docker installed, you don't have to install LaTeX if you don't want to.

```
make main.pdf
```
