IMAGE = ram/papers
# https://github.com/blang/latex-docker/blob/master/latexdockercmd.sh
# DOCKER_RUN = docker run --rm -i --user="$$(id -u):$$(id -g)" --net=none -v "$$PWD":/data:rw $(IMAGE)
DOCKER_RUN = docker run --rm -i --net=none -v "$$PWD":/data:rw $(IMAGE)

main.pdf: .make-timestamp_container main.tex
	$(DOCKER_RUN) pdflatex main.tex

.PHONY: container
container: .make-timestamp_container

.make-timestamp_container: Dockerfile
	touch "$@"
	docker build -f Dockerfile -t $(IMAGE)
